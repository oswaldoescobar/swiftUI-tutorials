//
//  NetworkManager.swift
//  SwiftUICombineMVVM
//
//  Created by Oswaldo Escobar on 10/02/23.
//  Copyright © 2023 iOS App Templates. All rights reserved.
//

import Foundation

class NetworkManager {
    
    func getPost(dataRetorno: @escaping([Brewery] )->()) {
        guard let url = URL(string: "https://api.openbrewerydb.org/breweries") else {
            print("URL NO CORRECTA")
            return
        }
        URLSession.shared.dataTask(with: url) { datos, respuesta, error in
            guard let data = datos, error == nil, let respuesta = respuesta as? HTTPURLResponse else {
                return
            }
            if respuesta.statusCode == 200 {
                do {
                    let posts = try JSONDecoder().decode([Brewery].self,from: data)
                    DispatchQueue.main.async {
                        dataRetorno(posts)
                    }
                } catch let error {
                    print ("Ha ocurrido un error > \(error.localizedDescription)")
                }
            }
        }.resume()
    }
}
